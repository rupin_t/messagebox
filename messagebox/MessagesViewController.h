//
//  MessagesViewController.h
//  messagebox
//
//  Created by rupin_t on 2/17/13.
//  Copyright (c) 2013 epitech-xlab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CouchCocoa/CouchCocoa.h>
#import <CouchCocoa/CouchUITableSource.h>
#import "ApiTools.h"

@interface MessagesViewController : UIViewController<CouchUITableDelegate, UITableViewDelegate>

@property (nonatomic) CouchDatabase *database;
@property (nonatomic) ApiTools *apiConnection;
@property (nonatomic) NSString *contact;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet CouchUITableSource *dataSource;

@property (nonatomic) int currentMessage;

- (void) useDatabase:(CouchDatabase *)database;
- (void)deleteCurrentMessage;

@end

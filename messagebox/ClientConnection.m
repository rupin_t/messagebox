//
//  ClientConnection.m
//  test
//
//  Created by Innovation Laboratory on 6/11/12.
//  Copyright (c) 2012 Epitech. All rights reserved.
//

#import "ClientConnection.h"

@implementation ClientConnection

@synthesize connection = _connection;
@synthesize datas = _datas;
@synthesize response = _response;
@synthesize errorResponse = _errorResponse;
@synthesize reachabilityResponse = _reachabilityResponse;

- (id)init
{
    if (self = [super init]) {
        [self setConnection:nil];
        [self setDatas:nil];
        [self setResponse:nil];
        [self setDatas:nil];
        [self setReachabilityResponse:nil];
    }
    return self;
}

- (void)checkReachabilityWithUrl:(NSString *)url withRes:(boolResponse)res
{
    _reachabilityResponse = res;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:0.4];
    [self setConnection:[[NSURLConnection alloc] initWithRequest:request delegate:self]];
}

+ (bool)checkReachabilityWithUrl:(NSString *)url
{
    NSURL* requestUrl = [[NSURL alloc] initWithString:url];
    NSURLRequest* request = [NSURLRequest requestWithURL:requestUrl cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
    
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    if (data) {
//        NSString* responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"%@", responseString);
        return true;
    }

    return false;
}

+ (NSData *)postRequestWithURL:(NSString *)url andData:(NSString *)data
{
    NSMutableData *postData = [[NSMutableData alloc] initWithLength:0];
    [postData appendData:[data dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%d", [postData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    return [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
}

+ (NSData *)getRequestWithURL:(NSString *)url
{
    NSURLRequest *request = [NSURLRequest requestWithURL:
                             [NSURL URLWithString:url]
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:60.0];
    return [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
}

- (void)postRequestWithURL:(NSString *)url andData:(NSString *)data andDelegate:(id)delegate
{
    NSMutableData *postData = [[NSMutableData alloc] initWithLength:0];
    [postData appendData:[data dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%d", [postData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    [self setConnection:[[NSURLConnection alloc] initWithRequest:request delegate:delegate]];
}

- (void)jsonPostRequestWithURL:(NSString *)url andJsonObject:(id)jsonObject andDelegate:(id)delegate
{
    NSData *data = [NSJSONSerialization dataWithJSONObject:jsonObject options:NSJSONWritingPrettyPrinted error:nil];    
    NSString *requestString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSMutableData *postData = [[NSMutableData alloc] initWithLength:0];
    [postData appendData:[requestString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                    [NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%d", [postData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    [self setConnection:[[NSURLConnection alloc] initWithRequest:request delegate:delegate]];
}

- (void)getJsonRequestWithURL:(NSString *)url andDelegate:(id)delegate
{
    NSURLRequest *request = [NSURLRequest requestWithURL:
                             [NSURL URLWithString:url]
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:60.0];
    [self setConnection:[[NSURLConnection alloc] initWithRequest:request delegate:delegate]];
}

#pragma mark NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _reachabilityResponse([NSNumber numberWithBool:YES]);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    _reachabilityResponse([NSNumber numberWithBool:NO]);
}

@end

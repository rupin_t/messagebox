//
//  MessagesViewController.m
//  messagebox
//
//  Created by rupin_t on 2/17/13.
//  Copyright (c) 2013 epitech-xlab. All rights reserved.
//

#import "MessagesViewController.h"
#import "NSString+Utils.h"
#import "DetailViewController.h"
#import "AppDelegate.h"

@implementation MessagesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSAssert(_database != nil, @"database is not set");
    NSAssert(_apiConnection != nil, @"apiConnection is not set");
    NSAssert(_contact != nil, @"contact is not set");

    CouchLiveQuery *query = [[[_database designDocumentWithName:@"default"] queryViewNamed:@"messages"] asLiveQuery];
    query.keys = @[_contact];

    _dataSource.query = query;
    _dataSource.labelProperty = @"sent_at";

}

- (void) useDatabase:(CouchDatabase *)database
{
    _database = database;

    CouchDesignDocument *design = [database designDocumentWithName:@"default"];
    [design defineViewNamed: @"messages" mapBlock:MAPBLOCK({
        NSArray *messages = [doc objectForKey:@"messages"];
        if (messages) {
            for (NSDictionary *message in messages) {
                emit([doc objectForKey:@"_id"], message);
            }
        }
    }) version: @"1.0"];

}

- (void)fetchImageAtUrl:(NSString *)url andRes:(idResponse)res
{
    NSString *path = [NSString stringWithFormat:@"%@%@", @"/tmp/", [url MD5]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path] == YES) {
        NSData *data = [fileManager contentsAtPath:path];
        res([UIImage imageWithData:data]);
    }
    else {
        [_apiConnection rawGetWithUrl:url andData:@"" andRes:^(NSData *data) {
            [fileManager createFileAtPath:path contents:data attributes:nil];
            res([UIImage imageWithData:data]);
        }];
    }
}

- (NSDictionary *)getDocumentFromIndexPath:(NSIndexPath *)indexPath
{
    CouchQueryRow *row = [_dataSource rowAtIndex:indexPath.row];
    NSArray *msgs = row.document.properties[@"messages"];
    return msgs[indexPath.row];
}

- (void)showErrorAlert:(NSString*)message forOperation:(RESTOperation*)op {
    NSLog(@"%@: op=%@, error=%@", message, op, op.error);
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    [app showAlert:message error:op.error fatal:false];
}

- (void)deleteCurrentMessage
{
    CouchQueryRow *row = [_dataSource rowAtIndex:_currentMessage];
    CouchDocument *document = row.document;
    
    NSMutableArray *messages = [document.properties[@"messages"] mutableCopy];
    [messages removeObjectAtIndex:_currentMessage];
    
    RESTOperation *op = [document putProperties:@{
                            @"_rev": document.properties[@"_rev"],
                            @"messages": messages
                         }];
    [op onCompletion:^{
        if (op.error)
            [self showErrorAlert:@"Couldn't delete the message" forOperation:op];
    }];
    [op start];
}

#pragma mark TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *message = [self getDocumentFromIndexPath:indexPath];
    [self fetchImageAtUrl:message[@"image_target"] andRes:^(UIImage *image) {
        cell.imageView.image = image;
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *message = [self getDocumentFromIndexPath:indexPath];
    
    DetailViewController *detailController = [[DetailViewController alloc] init];
    [detailController setDelegate:self];
    [detailController setMessage:message];
    _currentMessage = indexPath.row;
    [self.navigationController pushViewController:detailController animated:true];
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

//
//  NSString+Md5.m
//  test
//
//  Created by Innovation Laboratory on 6/13/12.
//  Copyright (c) 2012 Epitech. All rights reserved.
//

#import "NSString+Utils.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString(Utils)

- (NSString*)MD5
{
    const char *ptr = [self UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(ptr, strlen(ptr), md5Buffer);
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) 
        [output appendFormat:@"%02x",md5Buffer[i]];
    return output;
}

- (NSString *)sha512
{
    const char *ptr = [self UTF8String];
    unsigned char sha512Buffer[CC_SHA512_DIGEST_LENGTH];
    
    CC_SHA512(ptr, strlen(ptr), sha512Buffer);
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA512_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA512_DIGEST_LENGTH; i++) 
        [output appendFormat:@"%02x", sha512Buffer[i]];
    return output;
}

- (NSString *)append:(id)first, ...
{
    NSString *result = [self copy];
    id eachArg;
    va_list alist;
    if(first)
    {
        result = [[NSString alloc] initWithFormat:@"%@%@", result, first];
        va_start(alist, first);
        while ((eachArg = va_arg(alist, id)))
            result = [[NSString alloc] initWithFormat:@"%@%@", result, eachArg];
        va_end(alist);
    }
    return result;
}

@end

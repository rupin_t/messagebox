//
//  ApiTools.h
//  openDataApiTools
//
//  Created by rupin_t on 9/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClientConnection.h"
#import "NSString+Utils.h"

#define REFRESH_TIMEOUT 60
#define HOST @"http://127.0.0.1:3000"

@interface ApiTools : NSObject <NSURLConnectionDelegate>

@property (strong, nonatomic) NSMutableArray *connections;
@property (nonatomic) bool reachable;
@property (strong, nonatomic) NSString *host;
@property (nonatomic) bool autoSyncIsActive;
@property (nonatomic) bool locked;

- (ApiTools *)init;
- (void)didLoad;

- (bool)isOnline;

- (void)stopAutoSync;
- (void)startAutoSync;
- (void)autoSync;

- (NSString *)prepareBody:(NSString *)data;
- (NSString *)prepareUrl:(NSString *)url;
- (NSString *)prepareGet:(NSString *)data;
- (id)jsonResponse:(NSData *)data;

- (void)postWithUrl:(NSString *)url andData:(NSString *)data andRes:(idResponse)res;
- (void)getWithUrl:(NSString *)url andData:(NSString *)data andRes:(idResponse)res;

- (id)postWithUrl:(NSString *)url andData:(NSString *)data;
- (id)getWithUrl:(NSString *)url andData:(NSString *)data;

- (void)rawGetWithUrl:(NSString *)url andData:(NSString *)data andRes:(idResponse)res;
- (NSData *)rawGetWithUrl:(NSString *)url andData:(NSString *)data;

- (void)getNetworkStatus:(boolResponse)withRes andUrl:(NSString *)url;

@end

//
//  NSString+Md5.h
//  test
//
//  Created by Innovation Laboratory on 6/13/12.
//  Copyright (c) 2012 Epitech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Utils)

- (NSString *)MD5;
- (NSString *)sha512;
- (NSString *) append:(id) first, ...;

@end

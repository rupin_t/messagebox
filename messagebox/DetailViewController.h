//
//  DetailViewController.h
//  messagebox
//
//  Created by rupin_t on 2/17/13.
//  Copyright (c) 2013 epitech-xlab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessagesViewController.h"

@interface DetailViewController : UIViewController

@property (nonatomic) MessagesViewController *delegate;
@property (nonatomic) NSDictionary *message;

@property (strong, nonatomic) IBOutlet UITextView *messageContent;

- (IBAction)deleteMessage:(id)sender;

@end

//
//  ApiTools.m
//  openDataApiTools
//
//  Created by rupin_t on 9/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ApiTools.h"

@implementation ApiTools

@synthesize connections = _connections;
@synthesize reachable = _reachable;
@synthesize host = _host;
@synthesize autoSyncIsActive = _autoSyncIsActive;
@synthesize locked = _locked;

- (ApiTools *)init
{
    if (self = [super init]) {
        [self setConnections:[[NSMutableArray alloc] init]];
        [self setReachable:false];
        [self setHost:HOST];
        [self setAutoSyncIsActive:NO];
        [self setLocked:YES];
    }
    return self;
}

- (void)getNetworkStatus:(boolResponse)withRes andUrl:(NSString *)url
{
    if ([self locked]) {
        _reachable = [ClientConnection checkReachabilityWithUrl:url];
        _locked = NO;
        withRes([NSNumber numberWithBool:_reachable]);
    }
    else {
        ClientConnection *co = [[ClientConnection alloc] init];
        [co checkReachabilityWithUrl:url withRes:^(NSNumber *reachable) {
            _reachable = [reachable boolValue];
            NSLog(@"%@ reachability %@.", url, reachable);
            withRes(reachable);
        }];
    }
}

- (bool)isOnline
{
    return [self reachable];
}

- (id)jsonResponse:(NSData *)data
{
    NSError *error = nil;
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSUTF8StringEncoding error:&error];
    if (error) {
        NSLog(@"JsonResponse error: %@", error);
        return nil;
    }
    if (response && ![response isKindOfClass:[NSArray class]] && [response objectForKey:@"error"]) {
        NSLog(@"error: %@", [response objectForKey:@"msg"]);
        return nil;
    }
    return response;
}

- (NSString *)prepareUrl:(NSString *)url
{
    return [[NSString alloc] initWithFormat:@"%@%@", _host, url];
}

- (NSString *)prepareGet:(NSString *)data
{
    return [data stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
}

- (void)getWithUrl:(NSString *)url andData:(NSString *)data andRes:(idResponse)res
{
    if ([self isOnline]) {
        ClientConnection *co = [[ClientConnection alloc] init];
        [co setResponse:^(NSData *data) {
            res ([self jsonResponse:data]);
        }];
        url = [[NSString alloc] initWithFormat:@"%@%@", url, data];        
        [co getJsonRequestWithURL:[self prepareUrl:url] andDelegate:self];
        [[self connections] addObject:co];
        [[co connection] start];
    }
    else {
        NSLog(@"host is not reachable. %@ request failed.", url);
    }
}

- (void)rawGetWithUrl:(NSString *)url andData:(NSString *)data andRes:(idResponse)res
{
    ClientConnection *co = [[ClientConnection alloc] init];
    [co setResponse:^(NSData *data) {
        res (data);
    }];
    url = [[NSString alloc] initWithFormat:@"%@%@", url, data];
    [co getJsonRequestWithURL:url andDelegate:self];
    [[self connections] addObject:co];
    [[co connection] start];
}

- (NSData *)rawGetWithUrl:(NSString *)url andData:(NSString *)data
{
    url = [[NSString alloc] initWithFormat:@"%@%@", url, data];
    return [ClientConnection getRequestWithURL:[self prepareUrl:url]];
}

- (id)getWithUrl:(NSString *)url andData:(NSString *)data
{
    if ([self isOnline]) {
        url = [[NSString alloc] initWithFormat:@"%@%@", url, data];
        NSData *response = [ClientConnection getRequestWithURL:[self prepareUrl:url]];
        if (response)
            return [self jsonResponse:response];
    }
    else NSLog(@"host is not reachable. %@ request failed.", url);
    return nil;
}

- (NSString *)prepareBody:(NSString *)data
{
    return [data stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
}

- (id)postWithUrl:(NSString *)url andData:(NSString *)data
{
    if ([self isOnline]) {
        NSData *response = [ClientConnection postRequestWithURL:[self prepareUrl:url] andData:[self prepareBody:data]];
        if (response)
            return [self jsonResponse:response];
    }
    else NSLog(@"host is not reachable. %@ request failed.", url);
    return nil;
}

- (void)postWithUrl:(NSString *)url andData:(NSString *)data andRes:(idResponse)res
{
    if ([self isOnline]) {
        ClientConnection *co = [[ClientConnection alloc] init];
        [co setResponse:^(NSData *data) {
            res([self jsonResponse:data]);
        }];
        [co postRequestWithURL:[self prepareUrl:url] andData:[self prepareBody:data] andDelegate:self];
        [[self connections] addObject:co];
        [[co connection] start];
    }
    else {
        NSLog(@"host is not reachable. %@ request failed.", url);
    }
}

#pragma mark Auto Sync

- (void)stopAutoSync
{
    [self setAutoSyncIsActive:NO];
}

- (void)startAutoSync
{
    [self setAutoSyncIsActive:YES];
    [self autoSync];
}

- (void)autoSync
{
    if (![self autoSyncIsActive])
        return;

    [self getNetworkStatus:^(NSNumber *_) {
        [NSTimer scheduledTimerWithTimeInterval:REFRESH_TIMEOUT target:self selector:@selector(autoSync) userInfo:nil repeats:NO];
    } andUrl:_host];
}

#pragma mark Handle asynchronous data

- (ClientConnection *)searchConnection:(NSURLConnection *)connection
{
    for (ClientConnection *co in _connections) {
        if ([connection isEqual:[co connection]]) {
            return co;
        }
    }
    return nil;
}

- (void)removeConnection:(NSURLConnection *)connection
{
    for (ClientConnection *co in _connections) {
        if ([connection isEqual:[co connection]]) {
            [_connections removeObject:connection];
            return;
        }
    }
}

#pragma mark NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    ClientConnection *co = [self searchConnection:connection];
    
    if (co) {
        [co setDatas:[[NSMutableData alloc] initWithLength:0]];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    ClientConnection *co = [self searchConnection:connection];
    
    if (co) {
        [[co datas] appendData:data];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    ClientConnection *co = [self searchConnection:connection];
    if (co) {
        if ([co errorResponse])
            [co errorResponse](error);
        [self removeConnection:connection];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    ClientConnection *co = [self searchConnection:connection];
    
    if (co) {
        if ([co response]) [co response]([co datas]);
        [self removeConnection:connection];
    }
}

@end

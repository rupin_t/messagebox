//
//  DetailViewController.m
//  messagebox
//
//  Created by rupin_t on 2/17/13.
//  Copyright (c) 2013 epitech-xlab. All rights reserved.
//

#import "DetailViewController.h"

@implementation DetailViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    NSAssert(_delegate != nil, @"delegate is not set");
    NSAssert(_message != nil, @"message is not set");
    
    _messageContent.text = _message[@"body"];
}

- (IBAction)deleteMessage:(id)sender
{
    [_delegate deleteCurrentMessage];
    [self.navigationController popViewControllerAnimated:true];
}

@end

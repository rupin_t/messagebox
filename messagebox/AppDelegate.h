//
//  AppDelegate.h
//  messagebox
//
//  Created by rupin_t on 2/16/13.
//  Copyright (c) 2013 epitech-xlab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CouchCocoa/CouchCocoa.h>
#import "ApiTools.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) CouchDatabase *database;
@property (strong, nonatomic) ApiTools *apiConnection;

@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) IBOutlet UINavigationController *navigationController;

- (void)showAlert:(NSString *)message error:(NSError*)error fatal:(BOOL)fatal;

@end

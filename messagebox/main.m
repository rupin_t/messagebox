//
//  main.m
//  messagebox
//
//  Created by rupin_t on 2/16/13.
//  Copyright (c) 2013 epitech-xlab. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

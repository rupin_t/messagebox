//
//  AppDelegate.m
//  messagebox
//
//  Created by rupin_t on 2/16/13.
//  Copyright (c) 2013 epitech-xlab. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"
#import <TouchDB/TouchDB.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [_window addSubview:_navigationController.view];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];

    //Init of the work database
    gCouchLogLevel = 1;
        
    CouchTouchDBServer* server = [CouchTouchDBServer sharedInstance];
    NSAssert(!server.error, @"Error initializing TouchDBServer: %@", server.error);

    // Create the database on the first run of the app.
    _database = [server databaseNamed:@"message-box"];
    NSError* error;
    if (![_database ensureCreated: &error])
        [self showAlert:@"Couldn't create local database." error:error fatal:YES];
    _database.tracksChanges = YES;
    NSLog(@"Database as been created at <%@>", _database.URL);

    // Init the connection with the api (here it will be only one url)
    _apiConnection = [[ApiTools alloc] init];
    [_apiConnection setHost:@"https://go-text.me"];
    
    [_apiConnection getNetworkStatus:^(NSNumber *available) {
        if (![available boolValue]) {
            [self showAlert:@"Unable to synchronize messages online." error:nil fatal:false];
        }
        else { // so we are sure to be able to synchronize

            // Uncomment to reinit the db
//            CouchQuery *query = [_database getAllDocuments];
//            for (CouchQueryRow *row in query.rows) {
//                RESTOperation *op = [row.document DELETE];
//                [op wait];
//            }
            
            [_apiConnection getWithUrl:@"/tmp/messages.php" andData:@"" andRes:^(NSArray *response) {
                if (response) {
                    
                    NSMutableArray *toSave = [[NSMutableArray alloc] init];
                    for (NSDictionary *message in response) {
                        if ([toSave count] > 100) {
                            RESTOperation *op = [_database putChanges:toSave];
                            [op onCompletion: ^{
                                if (op.error)
                                    NSLog(@"Couldn't save the new message.");
                            }];
                            [op start];
                            [toSave removeAllObjects];
                        }
                        CouchDocument *document = [_database documentWithID:message[@"f"]];
                        
                        NSMutableDictionary *content = [document.properties mutableCopy];
                        
                        NSDictionary *m = @{
                            @"body": message[@"b"],
                            @"sent_at": message[@"ts"],
                            @"image_target": message[@"i"]
                        };
                        
                        if (document.properties[@"messages"] == nil) {
                            content[@"messages"] = @[m];
                        }
                        else {
                            NSMutableArray *ms = [document[@"messages"] mutableCopy];
                            [ms addObject:m];

                            NSArray *sorted = [ms sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
                                int first = [[RESTBody dateWithJSONObject:obj1[@"sent_at"]] timeIntervalSince1970];
                                int second = [[RESTBody dateWithJSONObject:obj2[@"sent_at"]] timeIntervalSince1970];
                                if (first > second)
                                    return (NSComparisonResult)NSOrderedAscending;
                                else if (first < second)
                                    return (NSComparisonResult)NSOrderedDescending;
                                else
                                    return (NSComparisonResult)NSOrderedSame;
                            }];

                            
                            content[@"messages"] = sorted;
                        }
                        [toSave addObject:content];
                    }
                    
                    RESTOperation *op = [_database putChanges:toSave];
                    [op onCompletion: ^{
                        if (op.error)
                            NSLog(@"Couldn't save the new message.");
                    }];
                    [op start];
                }
            }];
            
        }
    } andUrl:[_apiConnection host]];
    
    // Init the root view controller
    RootViewController *rootViewController = (RootViewController *)_navigationController.topViewController;
    
    [rootViewController useDatabase:_database];
    [rootViewController setApiConnection:_apiConnection];
    
    return YES;
}

// Showing an alert view in case of error
// If fatal, the application is then killed
- (void)showAlert:(NSString*)message error:(NSError*)error fatal:(BOOL)fatal
{
    if (error) {
        message = [NSString stringWithFormat: @"%@\n\n%@", message, error.localizedDescription];
    }
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: (fatal ? @"Fatal Error" : @"Error") message: message delegate: (fatal ? self : nil) cancelButtonTitle: (fatal ? @"Quit" : @"Sorry") otherButtonTitles: nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    exit(0);
}

@end

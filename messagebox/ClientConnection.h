//
//  ClientConnection.h
//  test
//
//  Created by Innovation Laboratory on 6/11/12.
//  Copyright (c) 2012 Epitech. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^dataResponse)(NSData *);
typedef void(^idResponse)(id);
typedef void(^boolResponse)(NSNumber *);
typedef void(^errorResponse)(NSError *);
typedef void(^DicReponse)(NSDictionary *);
typedef void(^ArrayResponse)(NSArray *);

#define NORES ^(id r){}

@interface ClientConnection : NSObject <NSURLConnectionDelegate>

@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *datas;
@property (strong, nonatomic) dataResponse response;
@property (strong, nonatomic) errorResponse errorResponse;

@property (strong, nonatomic) boolResponse reachabilityResponse;

- (id)init;
- (void)jsonPostRequestWithURL:(NSString *)url andJsonObject:(id)jsonObject andDelegate:(id)delegate;
- (void)getJsonRequestWithURL:(NSString *)url andDelegate:(id)delegate;
- (void)postRequestWithURL:(NSString *)url andData:(NSString *)data andDelegate:(id)delegate;
- (void)checkReachabilityWithUrl:(NSString *)url withRes:(boolResponse)res;

+ (bool)checkReachabilityWithUrl:(NSString *)url;
+ (NSData *)postRequestWithURL:(NSString *)url andData:(NSString *)data;
+ (NSData *)getRequestWithURL:(NSString *)url;

@end

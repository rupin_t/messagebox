//
//  RootViewController.m
//  messagebox
//
//  Created by rupin_t on 2/16/13.
//  Copyright (c) 2013 epitech-xlab. All rights reserved.
//

#import "RootViewController.h"
#import "AppDelegate.h"
#import "MessagesViewController.h"

@implementation RootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSAssert(_database != nil, @"database is not set");
    NSAssert(_apiConnection != nil, @"apiConnection is not set");
    
    CouchLiveQuery *query = [[_database getAllDocuments] asLiveQuery];
    query.descending = NO;

    _dataSource.query = query;
    _dataSource.labelProperty = @"_id";

    CouchQuery *q = [[_database designDocumentWithName:@"default"] queryViewNamed:@"messagesByContact"];
    for (CouchQueryRow *row in q.rows) {
        NSLog(@"%@", row.documentProperties);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) useDatabase:(CouchDatabase *)database
{
    _database = database;
}

- (void)showErrorAlert:(NSString*)message forOperation:(RESTOperation*)op {
    NSLog(@"%@: op=%@, error=%@", message, op, op.error);
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    [app showAlert:message error:op.error fatal:false];
}

#pragma mark - Couch table source delegate

- (void)couchTableSource:(CouchUITableSource *)source willUseCell:(UITableViewCell*)cell forRow:(CouchQueryRow *)row
{
}

#pragma mark TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MessagesViewController *messages = [[MessagesViewController alloc] init];
    [messages useDatabase:_database];
    [messages setApiConnection:_apiConnection];

    CouchQueryRow *row = [_dataSource rowAtIndex:indexPath.row];
    
    [messages setContact:row.document.properties[@"_id"]];
    [self.navigationController pushViewController:messages animated:YES];
    
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end

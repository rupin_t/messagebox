//
//  RootViewController.h
//  messagebox
//
//  Created by rupin_t on 2/16/13.
//  Copyright (c) 2013 epitech-xlab. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CouchCocoa/CouchUITableSource.h>
#import <CouchCocoa/CouchCocoa.h>
#import "ApiTools.h"

@interface RootViewController : UIViewController <CouchUITableDelegate>

@property (nonatomic) CouchDatabase *database;
@property (nonatomic) ApiTools *apiConnection;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet CouchUITableSource *dataSource;

- (void)useDatabase:(CouchDatabase *)database;

@end
